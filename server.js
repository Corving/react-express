import express from "express"
import route from './ui/route/route.js'
import db from './data/db/conf.js'

const app = express();
app.use(express.json())
app.use(route)
db.sync()
.then(console.log('Connexion à la bdd'))
.catch( error => console.log(error))

app.listen(8080, () => console.log("Port 8080"))