import Product from "../../domain/models/products.js"
import productValidation from "../../domain/models/validation/productValidation.js"

// Get all products
const getAll = (req, res) => {
    Product.findAll({
        attributes: {exclude: ["createdAt","updatedAt"]}
    })
    .then(product => {
        res.status(200).json(product)
    })
    .catch(error => res.status(500).json(error))
}

// Get one product
const getOne = (req, res) => {
    const {id} = req.params
    Product.findByPk(id,{
        attributes: {exclude: ["createdAt","updatedAt"]}
    })
    .then(product => {
        if(!product) return res.status(404).json({msg: "Not found"})
        res.status(200).json(product)
    })
    .catch(error => res.status(500).json(error))
}

// Create product
const createOne = (req, res) => {
    const {body} = req
    const {error} = productValidation(body)
    if(error) return res.status(401).json(error.details[0].message)

    Product.create({ ... body}).then(() => {
        res.status(201).json({msg: "Create Ressource"})
    }).catch(error => res.status(500).json(error))
};

// Update product
const updateOne = (req, res) => {
    const {id} = req.params
    const {body} = req
    Product.findByPk(id)
    .then(product => {
        if(!product) return res.status(404).json({msg: "Not found"})

        product.title = body.title
        product.price = body.price
        product.description = body.description
        product.save()
        .then(() => res.status(500).json({msg: "Update ressource"}))
        .catch(error => res.status(500).json(error))

    })
    .catch(error => res.status(500).json(error))

}

// delete one product
const deleteOne = (req, res) => {

    const {id} = req.params
    const {body} = req
    Product.destroy({where: {id: id}})
    .then(ressource =>{
        if(ressource === 0) return res.status(404).json({msg: "Not found"})
        res.status(500).json({msg: "Delete ressource"})
    }) 
    .catch(error => res.status(500).json(error))

}

export{getAll,getOne,createOne,updateOne,deleteOne}